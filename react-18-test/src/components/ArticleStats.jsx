import { getReadingTime } from '../helpers/getReadingTime.js';

export function ArticleStats({ showExplanation, text }) {
    const readingTime = getReadingTime(text);

    return (
        <div>
            {date}
            <h4>Reading time</h4>
            {showExplanation && (
                <p className='explanation'>
                    Reading time: Predicts the amount of time it will take to read this
                    article. Based on an average reading speed of 238 words per minute.
                </p>
            )}
            <p>{readingTime}</p>
        </div>
    );
}
